# How to

## Install

```bash
cargo install --path ./{app_name}
```

`{app_name}` can be `image-lsb`, `audio-lsb`, or `image-to-spectrogram`.

If you want to install in a specific directory, you can use the `--root` flag. For example:

```bash
cargo install --path ./image-lsb --root $HOME/.local/bin
```

## Usage

### Encode in image's pixels LSB

```bash
audio-lsb base_image.png encode -i message.txt -o output.png
```

### Decode from image's pixels LSB

```bash
audio-lsb base_image.png decode
```

### Encode & Decode in/from audio's LSB

Same as above but with `audio-lsb` command instead.


# TODO:

- Good CLI for `image-to-spectrogram`
- Documentation for `image-to-spectrogram`
