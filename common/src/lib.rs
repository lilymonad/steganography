use std::string::FromUtf8Error;

#[derive(Debug)]
pub enum CodecError {
    TextTooLong,
    DecodeError(FromUtf8Error),
}

pub trait BitsNumber {
    const MAX_PRECISION: usize;

    fn get_first_lsb(&self, precision: usize) -> u64;
    fn set_first_lsb(&mut self, bits: u64, precision: usize);
}

macro_rules! impl_bits_number {
    ($($t:ty : $v:expr),*) => {
        $(
            impl BitsNumber for $t {
                const MAX_PRECISION: usize = $v;

                fn get_first_lsb(&self, precision: usize) -> u64 {
                    *self as u64 & ((1 << precision) - 1) as u64
                }

                fn set_first_lsb(&mut self, bits: u64, precision: usize) {
                    let precision = precision.min(Self::MAX_PRECISION) as $t;
                    let mask = !((1 << precision) - 1) as $t;
                    *self &= mask;
                    *self |= bits as $t;
                }
            }
        )*
    };
    () => {
        
    };
}

impl_bits_number!(u8: 8, u16: 16, u32: 32, u64: 64, i8: 8, i16: 16, i32: 32, i64: 64);

impl BitsNumber for f32 {
    const MAX_PRECISION: usize = 23;

    fn get_first_lsb(&self, precision: usize) -> u64 {
        let bits = self.to_bits();
        bits as u64 & ((1 << precision) - 1) as u64
    }

    fn set_first_lsb(&mut self, nbits: u64, precision: usize) {
        let mut bits = self.to_bits();
        let precision = precision.min(Self::MAX_PRECISION);
        let mask = !((1 << precision) - 1);
        bits = (bits & mask) | nbits as u32;
        *self = f32::from_bits(bits);
    }
}

impl BitsNumber for f64 {
    const MAX_PRECISION: usize = 52;

    fn get_first_lsb(&self, precision: usize) -> u64 {
        let bits = self.to_bits();
        bits as u64 & ((1 << precision) - 1) as u64
    }

    fn set_first_lsb(&mut self, nbits: u64, precision: usize) {
        let mut bits = self.to_bits();
        let precision = precision.min(Self::MAX_PRECISION);
        let mask = !((1 << precision) - 1);
        bits = (bits & mask) | nbits as u64;
        *self = f64::from_bits(bits);
    }
}

pub fn encode_on<N: BitsNumber>(
    slice: &mut [N],
    text: &str,
) -> Result<(), CodecError> {
    if slice.len() / 4 < (text.len() + 1) {
        return Err(CodecError::TextTooLong);
    }
    for (subslice, c) in slice.chunks_mut(4).zip(text.bytes().chain(Some(0))) {
        subslice[0].set_first_lsb(((c >> 6) & 0b11) as u64, 2);
        subslice[1].set_first_lsb(((c >> 4) & 0b11) as u64, 2);
        subslice[2].set_first_lsb(((c >> 2) & 0b11) as u64, 2);
        subslice[3].set_first_lsb(((c >> 0) & 0b11) as u64, 2);
    }

    Ok(())
}

pub fn decode_from<N: BitsNumber>(slice: &[N]) -> Result<String, CodecError> {
    let mut text_bytes = Vec::new();
    for subslice in slice.chunks(4) {
        if subslice.len() < 4 {
            break;
        }

        let c = (subslice[0].get_first_lsb(2) as u8 & 0b11) << 6
            | (subslice[1].get_first_lsb(2) as u8 & 0b11) << 4
            | (subslice[2].get_first_lsb(2) as u8 & 0b11) << 2
            | (subslice[3].get_first_lsb(2) as u8 & 0b11);

        if c == 0 {
            break;
        }

        text_bytes.push(c);
    }

    String::from_utf8(text_bytes).map_err(CodecError::DecodeError)
}
