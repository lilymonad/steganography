use clap::{Parser, Subcommand};
use claxon::FlacReader;
use flacenc::{component::BitRepr, error::Verify};
use std::{fs::File, io::Read, path::PathBuf};

fn encode(mut reader: FlacReader<File>, output_file: PathBuf, message: String) {
    let mut samples = reader
        .samples()
        .map(|s| s.ok())
        .collect::<Option<Vec<_>>>()
        .expect("Could not read samples");

    common::encode_on(&mut samples, &message).expect("Encoding error");

    // write encoded message to file
    let (channels, bits_per_sample, sample_rate) = (
        reader.streaminfo().channels,
        reader.streaminfo().bits_per_sample,
        reader.streaminfo().sample_rate,
    );
    let config = flacenc::config::Encoder::default()
        .into_verified()
        .expect("Config data error");
    let source = flacenc::source::MemSource::from_samples(
        &samples,
        channels as usize,
        bits_per_sample as usize,
        sample_rate as usize,
    );
    let flac_stream = flacenc::encode_with_fixed_block_size(&config, source, config.block_size)
        .expect("Encoding error");
    let mut sink = flacenc::bitsink::ByteSink::new();
    flac_stream.write(&mut sink).expect("Write error");
    std::fs::write(output_file, sink.as_slice()).expect("Write file error");
}

fn decode(mut reader: FlacReader<File>) {
    let samples = reader
        .samples()
        .map(|s| s.ok())
        .collect::<Option<Vec<_>>>()
        .expect("Could not read samples");
    let message = common::decode_from(&samples).expect("Decoding error");
    println!("{}", message);
}

#[derive(Parser)]
struct Args {
    base_file: PathBuf,
    #[command(subcommand)]
    command: Command,
}

#[derive(Subcommand)]
enum Command {
    Encode {
        #[arg(
            short = 'i',
            default_value_os_t = PathBuf::from("-"),
            long_help = "Read message from given file (or stdin if -)",
        )]
        message_file: PathBuf,
        #[arg(short, long_help = "Output file", default_value_os_t = PathBuf::from("encoded.flac"))]
        output_file: PathBuf,
    },
    Decode,
}

fn main() {
    let args = Args::parse();
    let reader = FlacReader::new(std::fs::File::open(&args.base_file).unwrap()).unwrap();

    match args.command {
        Command::Encode {
            message_file,
            output_file,
        } => {
            let mut message = String::new();
            match message_file.to_str() {
                Some("-") => std::io::stdin()
                    .lock()
                    .read_to_string(&mut message)
                    .expect("Could not read stdin"),
                _ => File::open(message_file)
                    .expect("Could not open file")
                    .read_to_string(&mut message)
                    .expect("Could not read file"),
            };
            encode(reader, output_file, message);
        }
        Command::Decode => {
            decode(reader);
        }
    }
}
