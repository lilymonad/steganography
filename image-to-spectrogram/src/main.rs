use hound;
use std::{env, f64::consts::PI, time::Duration};

fn populate_samples<W: std::io::Write + std::io::Seek>(
    img: &image::ImageBuffer<image::Luma<u16>, Vec<u16>>,
    out: &mut hound::WavWriter<W>,
    sample_rate: u32,
    duration: Duration,
) {
    let samples = (sample_rate as f64 * duration.as_secs_f64()) as usize;
    let samples_per_pixel = samples as usize / img.width() as usize;
    if samples_per_pixel == 0 {
        panic!("Image is too wide for the given duration");
    }
    let mut result = Vec::with_capacity(samples);
    let c = 20000.0 / img.height() as f64;
    let mut max_sum: f64 = 0.0;
    for sample in 0..samples {
        let x = ((sample / samples_per_pixel) as u32).min(img.width() - 1);
        let mut sum: f64 = 0.0;
        for y in 0..img.height() {
            let pixel = img.get_pixel(x, y);
            let volume= pixel[0] as f64 / u16::MAX as f64;
            let f = (c * (img.height() - y + 1) as f64).floor();
            let si = (f * PI * 2.0 * sample as f64 / sample_rate as f64).sin();
            sum += volume * si;
        }
        max_sum = max_sum.max(sum.abs());
        result.push(sum);
    }

    for sample in result {
        let sf = (sample * i16::MAX as f64) / max_sum;
        if sf.floor() > i16::MAX as f64 {
            panic!("Sample value too large {sf}");
        }
        let s = sf.floor() as i16;
        out.write_sample(s).expect("Failed to write sample");
    }
}

fn main() {
    let path = env::args().nth(1).expect("No image path given");
    let img = image::open(path).expect("Failed to open image").into_luma16();

    let (channels, bits_per_sample, sample_rate) = (1, 16, 44100);
    let spec = hound::WavSpec {
        channels,
        sample_rate,
        bits_per_sample,
        sample_format: hound::SampleFormat::Int,
    };
    let mut writer =
        hound::WavWriter::create("output.wav", spec).expect("Failed to create WAV file");
    populate_samples(&img, &mut writer, 44100, Duration::from_secs(10));
    writer.finalize().expect("Failed to finalize WAV file");
}
