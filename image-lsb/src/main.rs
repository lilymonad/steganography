use clap::{Parser, Subcommand};
use common::BitsNumber;
use image::{DynamicImage, ImageBuffer, Pixel};
use std::{
    fs::File,
    io::Read as _,
    ops::{Deref, DerefMut},
    path::PathBuf,
};

fn encode_on<
    N: BitsNumber,
    T: Pixel<Subpixel = N>,
    C: DerefMut<Target = [T::Subpixel]> + AsMut<[T::Subpixel]>,
>(
    image: &mut ImageBuffer<T, C>,
    text: &str,
) {
    let mut samples = image.as_flat_samples_mut();
    let slice = samples.as_mut_slice();
    common::encode_on(slice, text).expect("Error: Text too long.");
}

fn decode_from<
    N: BitsNumber,
    T: Pixel<Subpixel = N>,
    C: Deref<Target = [T::Subpixel]> + AsRef<[T::Subpixel]>,
>(
    image: &ImageBuffer<T, C>,
) -> String {
    let samples = image.as_flat_samples();
    let slice = samples.as_slice();

    common::decode_from(slice).expect("Error: Invalid text.")
}

#[derive(Parser)]
struct Args {
    base_file: PathBuf,
    #[command(subcommand)]
    command: Command,
}

#[derive(Subcommand)]
enum Command {
    Encode {
        #[arg(
            short = 'i',
            default_value_os_t = PathBuf::from("-"),
            long_help = "Read message from given file (or stdin if -)",
        )]
        input_file: PathBuf,
        #[arg(short, long_help = "Output file")]
        output_file: Option<PathBuf>,
    },
    Decode,
}

fn main() {
    let args = Args::parse();

    match args.command {
        Command::Encode {
            input_file,
            output_file,
        } => {
            let mut text = String::new();
            match input_file.to_str() {
                Some("-") => std::io::stdin()
                    .read_to_string(&mut text)
                    .expect("Error: Could not read stdin."),
                _ => File::open(input_file)
                    .expect("Error: Could not open file.")
                    .read_to_string(&mut text)
                    .expect("Error: Could not read file."),
            };
            let mut image = image::open(&args.base_file).unwrap();
            match &mut image {
                DynamicImage::ImageRgb32F(image) => encode_on(image, &text),
                DynamicImage::ImageRgba32F(image) => encode_on(image, &text),
                DynamicImage::ImageRgb8(image) => encode_on(image, &text),
                DynamicImage::ImageRgba8(image) => encode_on(image, &text),
                DynamicImage::ImageLuma8(image) => encode_on(image, &text),
                DynamicImage::ImageLumaA8(image) => encode_on(image, &text),
                DynamicImage::ImageRgb16(image) => encode_on(image, &text),
                DynamicImage::ImageRgba16(image) => encode_on(image, &text),
                DynamicImage::ImageLuma16(image) => encode_on(image, &text),
                DynamicImage::ImageLumaA16(image) => encode_on(image, &text),
                _ => {
                    panic!("Error: Invalid image type.");
                }
            }

            let output_path = output_file.unwrap_or_else(|| {
                PathBuf::from(format!("encoded_{}.png", args.base_file.display()))
            });
            image.save(output_path).unwrap();
        }
        Command::Decode => {
            let image = image::open(&args.base_file).unwrap();
            let text = match &image {
                DynamicImage::ImageRgb32F(image) => decode_from(image),
                DynamicImage::ImageRgba32F(image) => decode_from(image),
                DynamicImage::ImageRgb8(image) => decode_from(image),
                DynamicImage::ImageRgba8(image) => decode_from(image),
                DynamicImage::ImageLuma8(image) => decode_from(image),
                DynamicImage::ImageLumaA8(image) => decode_from(image),
                DynamicImage::ImageRgb16(image) => decode_from(image),
                DynamicImage::ImageRgba16(image) => decode_from(image),
                DynamicImage::ImageLuma16(image) => decode_from(image),
                DynamicImage::ImageLumaA16(image) => decode_from(image),
                _ => {
                    panic!("Error: Invalid image type.");
                }
            };
            println!("{}", text);
        }
    }
}
